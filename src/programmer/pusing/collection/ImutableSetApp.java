package programmer.pusing.collection;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class ImutableSetApp {
    public static void main(String[] args) {
        // membuat set imutable kosong
        Set<String> empty = Collections.emptySet();
        // membuat set imutable berisi 1 data saja
        Set<String> one = Collections.singleton("Abid");
        // merubah set mutable ke imutable
        Set<String> mutable = new HashSet<>();
        mutable.add("Abid");
        mutable.add("Wiratama");
        Set<String> imutable = Collections.unmodifiableSet(mutable);
        // membuat set dengan memasukkan element secara langsung
        // Set<String> element = Set.of("Abid","Wiratama"); eror karena requirement java 9
        Set<String> element = new HashSet<>(Arrays.asList("Abid","Wiratama"));
        element.add("COba");


    }
}

package programmer.pusing.collection;

import programmer.pusing.collection.data.Person;

import java.util.List;

public class MutableApp {
    public static void main(String[] args) {
        Person person = new Person("Abid");

        person.addHobby("Game");
        person.addHobby("Coding");

        // seseorang tidak sengaja menambahkan data ke dalam list bisa merusak data
        // maka dari itu kita perlu menggunakan imutable list yaitu list yang tidak bisa diubah datanya
        // untuk jaga2 jika seseorang tidak sengaja menambahkan data ke dalam list
        doSomethingWithHobbies(person.getHobbies());

        for(String hobby : person.getHobbies()){
            System.out.println(hobby);
        }
    }

    public static void doSomethingWithHobbies(List<String> hobbies){
        // menambahkan data yang seharusnya tidak ditambahkan
        hobbies.add("Bukan Hobby");
    }

}

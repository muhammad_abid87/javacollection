package programmer.pusing.collection;

import java.util.Arrays;
import java.util.Iterator;


public class IterableApp {
    public static void main(String[] args) {
        Iterable<String> names = Arrays.asList("Abid","Wirajumbo");


        // menampilkan data dapat dilakukan dengan foreach setelah java 5
        for(String name : names){
            System.out.println(name);
        }

        // namun sebelum java 5 untuk menampilkan collection harus menggunakan iterator
        Iterator<String> iterator = names.iterator();
        System.out.println("==ITERATOR==");
        while(iterator.hasNext()){ // cek apakah ada data selanjutnya
            String name = iterator.next(); // collect data menggunakan method next()
            System.out.println(name);
        }




    }
}

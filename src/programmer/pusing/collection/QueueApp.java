package programmer.pusing.collection;

import java.util.ArrayDeque;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

public class QueueApp {
    public static void main(String[] args) {
        // implementasi dari interface queue ada 3 yaitu
        // 1. ArrayDequeue adalah implementasi dari queue yaitu queue dengan menggunakan array sehingga mempunyai
        // batasan jumlah yang dapat diisi namun jika add lebih dari jumlah array nya maka array akan melakukan grow
        Queue<String> queue = new ArrayDeque<>(10);
        // 2. LinkedList adalah implementasi yang menggunakan double linked list, yang inti perbedaan dari array deque adalah
        // jika arraydequeue mempunyai batasan maka linked list tidak
//        Queue<String> queue = new LinkedList<>();
        // 3. PriorityQueue adalah implementasi dari interface queue yang akan melakukan sorting terlebih dahulu sebelum
        // dilakukan pengambilan data sehingga data yang ditampilkan akan urut secara asc
//        Queue<String> queue = new PriorityQueue<>();


//        for(int i = 0; i < 10; i++){
//            queue.add(String.valueOf(i));
//        }

        queue.add("Muhammad");
        queue.add("Abid");
        queue.add("Wiratama");

        // poll digunakan untuk mengambil data pertama yang di add karena queue menggunakan prinsip FIFO
        // poll akan mengambil data lalu menghapus data tersebut dari antrian
        for(String next = queue.poll(); next != null; next = queue.poll()){
            System.out.println(next);
        }
        // sesudah kita lakukan poll pada semua isi di dalam queue maka queue tersebut akan kosong
        // kita dapat membuktikannya menggunakan size() yaitu untuk mengitung jumlah elemen yang ada dalam queue
        System.out.println(queue.size());

    }
}

// method yang ada di queue
// 1. add = menambahkan ke antrian, dan jika sudah melebihi total data yang bisa ditampung maka akan throw exception
// 2. offer = sama seperti add untuk menambah data, namun jika melebih data yang bisa ditampung maka return false
// 3. remove = mengambil dan menghapus data yang pertama kali di add, tetapi jika data sudah kosong maka akan throw exception
// 4. poll = sama seperti remove namun perbedaannya jika datanya sudah kosong maka return null
// 5. element = mengambil data pertama namun tidak menghapus, namun jika data nya kosong maka throw exception
// 6. peek = sama seperti element perbedaannya hanya jika data nya kosong maka return null


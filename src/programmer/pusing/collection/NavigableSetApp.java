package programmer.pusing.collection;

import java.util.Arrays;
import java.util.Collections;
import java.util.NavigableSet;
import java.util.TreeSet;

public class NavigableSetApp {
    public static void main(String[] args) {
        // menampilkan set dengan asc
        NavigableSet<String> names = new TreeSet<>(Arrays.asList("Muhammad","Abid","Wiratama","Rizka","Amalia"));
        // jika kita ingin membalik urutan menjadi desc
        NavigableSet<String> namesReverse = names.descendingSet();
        // jika kita ingin mengambil value set dari elemen pertama sampai elemen x
        NavigableSet<String> muhammadFromHead = names.headSet("Muhammad",true);
        // namun, jika kita ingin mengambil value set dari elemen terakhir sampai elemen x
        NavigableSet<String> muhammadFromLast = names.tailSet("Muhammad",true);
        // jika kita ingin set yang kita buat tidak dapat dirubah adalah menggunakan
        NavigableSet<String> imutableNavSet = Collections.unmodifiableNavigableSet(names);
        //  jika kita coba add pada imutableNavSet maka akan terjadi eror
            imutableNavSet.add("Coba Add Pada Imutable Nav set"); // pasti akan terjadi eror 'UnsupportedOperationException'

        for(String val : imutableNavSet){
            System.out.println(val);
        }



    }
}

package programmer.pusing.collection;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

public class SetApp {
    public static void main(String[] args) {
        // perbedaan mendasar set dengan list adalah pada set tidak dapat menambahkan data duplikat
        //      perbedaan lain dari list adalah list mempunyai index dan set tidak
        // implementasi dari set ada 3 namun yang sering digunakan adalah hashset dan linkedhashset
        // perbedaan hashset dan linkedhashset adalah pada linkedhashset data yang ditambahkan jika
        //      ditampikan akan dipastikan urut sedangkan hashset tidak

//        Set<String> names = new HashSet<>();
        Set<String> names = new LinkedHashSet<>();
        // add duplikat data namun yang di add hanya yang unik
        names.add("Abid");
        names.add("Wiratama");
        names.add("Muhammad");
        names.add("Abid");
        names.add("Wiratama");
        names.add("Muhammad");

        for(String name : names){
            System.out.println(name);
        }
    }
}

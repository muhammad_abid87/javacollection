package programmer.pusing.collection;

import java.util.EnumSet;

public class EnumSetApp {

    // enumset digunakan untuk membuat set untuk enum
    public static enum Gender {
        MALE,FEMALE,NOT_MENTION;
    }
    public static void main(String[] args) {
//        EnumSet<Gender> genders = EnumSet.allOf(Gender.class);
        EnumSet<Gender> genders = EnumSet.of(Gender.FEMALE);
        for(Object gender : genders ){
            System.out.println(gender);
        }
    }
}

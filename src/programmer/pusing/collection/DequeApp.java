package programmer.pusing.collection;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;

public class DequeApp {
    public static void main(String[] args) {

        // perbedaan interface deque yang merupakan turunan dari queue adalah LIFO dan FIFO
        // LIFO
        Deque<String> stack = new LinkedList<>();

        stack.offerLast("Stack 1");
        stack.offerLast("Stack 2");
        stack.offerLast("Stack 3");

        System.out.println(stack.pollLast());
        System.out.println(stack.pollLast());
        System.out.println(stack.pollLast());

        // FIFO
        Deque<String> queue = new LinkedList<>();

        queue.offerLast("Antrian 1");
        queue.offerLast("Antrian 2");
        queue.offerLast("Antrian 3");

        System.out.println(queue.pollFirst());
        System.out.println(queue.pollFirst());
        System.out.println(queue.pollFirst());
    }
}

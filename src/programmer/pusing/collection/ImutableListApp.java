package programmer.pusing.collection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ImutableListApp {
    public static void main(String[] args) {
        // imutable list dengan satu data saja
        List<String> one = Collections.singletonList("Abid");
        // imutable list dengan empty list
        List<String> empty = Collections.emptyList();
        // mengubah mutable list menjadi imutable
        List<String> mutable = new ArrayList<>();
        mutable.add("abid");
        mutable.add("Wiratama");
        List<String> imutable = Collections.unmodifiableList(mutable);
        imutable.add("coba rubah"); // akan error karena list 'imutable' sudah imutable

        // menambahkan data ke list imutable
        List<String> element = Arrays.asList("Abid", "wiratama");
        element.add("s");
    }
}

package programmer.pusing.collection;

import java.util.ArrayList;
import java.util.List;

public class ListApp {
    public static void main(String[] args) {
        // array list best practice nya digunakan jika kita perlu menampilkan data dan merubah data
        // jika kita hanya nambah data dan iterasi best practice nya menggunakan linked list

        List<String> names = new ArrayList<>();
        //tambah data
        names.add("Abid");
        names.add("Wiratama");
        names.add("Muhammad");
        // ubah data
        names.set(0,"Abidin");
        //delete data
        names.remove(0);
        // tampilkan data pada index ke n
        names.get(0);

        for(String name : names){
            System.out.println(name);
        }




    }
}

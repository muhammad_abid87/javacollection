package programmer.pusing.collection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public class CollectionApp {
    public static void main(String[] args) {
        Collection<String> collection = new ArrayList<>();
        collection.add("Abid");
        collection.add("Wiratama");
        collection.addAll(Arrays.asList("Sangat baik","Biasalah"));

        for(String value : collection){
            System.out.println(value);
        }

        System.out.println("Remove");
        collection.remove("Abid");
        collection.removeAll(Arrays.asList("Wiratama","Sangat baik"));

        for(String value : collection){
            System.out.println(value);
        }

        // check apakah ada data ini di collection => contains(untuk satu data), containsAll(untuk banyak data)
        System.out.println(collection.contains("Abid"));
        System.out.println(collection.contains("Biasalah"));
        System.out.println(collection.containsAll(Arrays.asList("Abid","Biasalah")));

    }
}

package programmer.pusing.collection;

import programmer.pusing.collection.data.Person;
import programmer.pusing.collection.data.PersonComparator;

import java.util.Collections;
import java.util.SortedSet;
import java.util.TreeSet;

public class SortedSetApp {
    public static void main(String[] args) {
        SortedSet<Person> people = new TreeSet<>(new PersonComparator());
        people.add(new Person("Abid"));
        people.add(new Person("Wiratama"));
        people.add(new Person("Jumbo"));

        // imutable sorted set
        SortedSet<Person> imutable = Collections.unmodifiableSortedSet(people);
        imutable.add(new Person("Banget"));

        for(Person val : people){
            System.out.println(val.getName());
        }
    }

}
